
CREATE TYPE mood AS ENUM ('jun', 'middle', 'senior', 'lead');
CREATE TABLE stuff(
	id SERIAL PRIMARY KEY,
	surname VARCHAR(20) NOT NULL,
	birthday DATE,
	on_work DATE,
	post VARCHAR(20),
	level mood,
	salary INT,
	office_id INT NOT NULL,
	is_driver BOOLEAN
);

INSERT INTO stuff(surname, birthday, on_work, post, level, salary, office_id, is_driver)
VALUES
('Ivanov', '2000-12-21', '2020-11-25', 'manager', 'jun', 25000, 14, True),
('Petrov', '1980-01-11', '2018-01-20', 'IT', 'middle', 90000, 9, True),
('Coluga', '1987-01-13', '2019-06-03', 'HR', 'middle', 60000, 3, False),
('Trueman', '1985-02-07', '2015-03-12', 'director IT', 'senior', 170000, 9, False),
('Shir', '1990-12-21', '2023-03-12', 'director', 'senior', 180000, 7, True);


CREATE TABLE offices(
	id INT PRIMARY KEY,
	office_name VARCHAR(20),
	head_surname VARCHAR(20),
	stuff_count INT
);

INSERT INTO offices (id, office_name, head_surname, stuff_count)
VALUES
(14, 'Sales', 'Romus', 3),
(3, 'HR', 'Nibis', 1),
(9, 'IT', 'Trueman', 2),
(7, 'Head', 'Shir', 1);


CREATE TYPE liter As ENUM('A', 'B', 'C', 'D', 'E');
CREATE TABLE grade(
	id SERIAL PRIMARY KEY,
	surname VARCHAR(20),
	year INT,
	quarter INT,
	grade liter
);

INSERT INTO grade (surname, year, quarter, grade)
VALUES
('Romus', 2023, 1, 'E'),
('Nibis', 2023, 1, 'C'),
('Trueman', 2023, 1, 'A'),
('Shir', 2023, 1, 'A'),
('Ivanov', 2023, 1, 'B');

INSERT INTO stuff(surname, birthday, on_work, post, level, salary, office_id, is_driver)
VALUES
('Gurov', '2001-11-01', '2023-02-10', 'DA', 'jun', 29000, 2, True),
('Levina', '1981-12-31', '2023-02-10', 'head', 'senior', 780000, 2, True),
('Slowpok', '1989-05-07', '2023-02-10', 'DA', 'middle', 65000, 2, False);

INSERT INTO offices (id, office_name, head_surname, stuff_count)
VALUES
(2, 'Интеллектуальный DA', 'Levina', 3);

INSERT INTO grade (surname, year, quarter, grade)
VALUES
('Gurov', 2023, 1, 'C'),
('Levina', 2023, 1, 'C' ),
('Slowpok', 2023, 1, 'C');

# 6.1 Уникальный номер сотрудника, его ФИО и стаж работы – для всех сотрудников компании

SELECT id, 
		surname,
		(CURRENT_DATE - on_work) / 365 as exp 
FROM stuff;

6.2 Уникальный номер сотрудника, его ФИО и стаж работы – только первых 3-х сотрудников

SELECT id, 
		surname,
		(CURRENT_DATE - on_work) / 365 as exp 
FROM stuff
LIMIT 3;

6.3 Уникальный номер сотрудников - менеджеров

SELECT id 
FROM stuff
WHERE post LIKE '%manager%';

6.4 Выведите номера сотрудников, которые хотя бы за 1 квартал получили оценку D или E

SELECT st.id
FROM stuff as st
JOIN grade as gr ON st.surname = gr.surname
WHERE gr.grade = 'D' OR gr.grade = 'E';

6.5 Выведите самую высокую зарплату в компании.

SELECT MAX(salary)
FROM stuff;

6.6 * Выведите название самого крупного отдела

SELECT office_name
FROM offices
ORDER BY stuff_count DESC
LIMIT 1;

6.7 * Выведите номера сотрудников от самых опытных до вновь прибывших

SELECT id
FROM stuff
ORDER BY (CURRENT_DATE - on_work) DESC;
