package org.example
import scala.io.StdIn.readLine

object App {
  
  def main(args : Array[String]) {
    var textPrint = "Hello World!"
    println(textPrint)
    println(textPrint.reverse)
    println(textPrint.replace("!", ""))

    println("Ввидите годовой доход до вычета налога:")
    val yearSalary = readLine().toInt
    println("Ввидите премии:")
    val bonus = readLine().toInt
    println("Компенсацию обедов")
    val foodPay = readLine().toInt
    var sum = yearSalary + bonus + foodPay
    println(s"Сумма $sum")


    val firstList = List(100, 800, 14, 157, 2)

    for (n <- firstList)
      {
        if (n >=100 && n < 800) println(f"$n middle")
      }

    def addInt(a: List[Int]): Int = {
      return a.sum
    }

    println(addInt(firstList))
  }

}
