package org.example

object App2 {

  def main(args : Array[String]) {
    val avgMarket = 170
    val salary = List(100, 200, 500, 111, 125, 148, 197, 314)
    var midle = List[Int]()
    for (n <- salary)
      {
        if ((n >= 100) && (n <= 200))
        {
          midle = n +: midle
        }
      }
      println(midle)

    val avgSalary = midle.sum / midle.size
    println(avgSalary)
    val avgSalaryVar: Double = 1 - avgSalary.toDouble / avgMarket.toDouble
    println(avgSalaryVar)

    def recurse(n: Int): Int = {
      if (n <= 0) 1
      else {
        var fact = n * recurse(n-1)
        fact
      }
    }
    println(recurse(5))

  }

}
